package com.hexb.core.annotation

import org.springframework.core.annotation.AliasFor

import java.lang.annotation.*

/**
 * 用于标记字典型枚举
 * */
@Target([ElementType.TYPE, ElementType.FIELD])
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@interface ErrorCode {

    @AliasFor("name")
    String value() default ""

    @AliasFor("value")
    String name() default ""

    /**
     *模块名
     * */
    String module() default ""

    String description() default ""

}