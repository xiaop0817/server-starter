package com.hexb.core.annotation

import java.lang.annotation.*

/**
 * @Package : com.hexb.core.annotation
 * @Author : hexb 
 * @Date : 2018-08-17 15:20
 */
@Target([ElementType.FIELD, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface UpdateIgnore {

}