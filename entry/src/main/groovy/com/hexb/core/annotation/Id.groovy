package com.hexb.core.annotation

import org.springframework.core.annotation.AliasFor

import java.lang.annotation.*

/**
 * @Package : com.hexb.mt.annotations
 * @Author : hexb 
 * @Date : 2018-08-11 16:34
 */

@Target([ElementType.FIELD, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@interface Id {

    @AliasFor('value')
    String column() default ''

    @AliasFor('column')
    String value() default ''



}