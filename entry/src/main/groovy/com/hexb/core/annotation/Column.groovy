package com.hexb.core.annotation

import org.springframework.core.annotation.AliasFor

import java.lang.annotation.*

/**
 * @Package : com.hexb.core.annotation
 * @Author : hexb 
 * @Date : 2018-08-16 10:51
 */
@Target([ElementType.FIELD, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface Column {

    @AliasFor('value')
    String name() default ''

    @AliasFor('name')
    String value() default ''

    String jdbcType() default ''

    String javaType() default ''

}