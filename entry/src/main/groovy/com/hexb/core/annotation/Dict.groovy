package com.hexb.core.annotation

import org.springframework.core.annotation.AliasFor

import java.lang.annotation.ElementType
import java.lang.annotation.Inherited
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * 用于标记字典型枚举
 * */
@Target([ElementType.TYPE, ElementType.FIELD])
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@interface Dict {

    @AliasFor("name")
    String value() default ""

    @AliasFor("value")
    String name() default ""

    String description() default ""

}