package com.hexb.core.annotation

import com.hexb.core.model.enums.IDType
import org.springframework.core.annotation.AliasFor

import java.lang.annotation.*

/**
 * @Package : com.hexb.mt.annotations
 * @Author : hexb 
 * @Date : 2018-08-11 16:29
 */
@Target([ElementType.TYPE, ElementType.ANNOTATION_TYPE])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface Table {

    @AliasFor('value')
    String name() default ''

    @AliasFor('name')
    String value() default ''

    String orderBy() default 'createTime'

    IDType idType() default IDType.AUTO_INCREMENT
}