package com.hexb.core.annotation

import java.lang.annotation.Documented
import java.lang.annotation.ElementType
import java.lang.annotation.Inherited
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * @Package : com.hexb.core.annotation
 * @Author : hexb 
 * @Date : 2018-08-17 15:20
 */
@Target([ElementType.FIELD, ElementType.METHOD])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface InsertIgnore {

}