package com.hexb.core.utils

import com.hexb.core.model.ValueEnum

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb 
 * @Date : 2018-08-3 14:44
 */
class ValueEnumHelper {

    static <E extends ValueEnum<E>> List<E> splitStringToEnum(String s, Class<E> clazz) {
        ValueEnumHelper.splitStringToEnum(s, clazz, ',')
    }

    static <E extends ValueEnum<E>> List<E> splitStringToEnum(String s, Class<E> clazz, String splitChar) {
        s?.split(splitChar)
                .collect { it as Integer }
                .collect { ValueEnumUtils.getEnumByValue(clazz, it) }
                .findAll { it }
    }
}
