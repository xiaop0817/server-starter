package com.hexb.core.utils

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb 
 * @Date : 2018-08-31 11:47
 */
class DoubleHelper {

    /**
     * 四舍五入，保留2位
     * */
    static double fixed(Double d) {
        fixed(d, 2)
    }

    /**
     * 四舍五入，保留len位
     * */
    static double fixed(Double d, int len) {
        new BigDecimal(d).setScale(len, BigDecimal.ROUND_HALF_UP).toDouble()
    }

}
