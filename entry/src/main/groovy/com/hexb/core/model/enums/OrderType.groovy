package com.hexb.core.model.enums

/**
 * @Package : com.hexb.core.model.enums
 * @Author : hexb 
 * @Date : 2018-08-19 10:14
 */
enum OrderType {
    ASC, DESC
}