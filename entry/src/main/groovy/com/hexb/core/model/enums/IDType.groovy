package com.hexb.core.model.enums

/**
 * @Package : com.hexb.core.model.enums
 * @Author : hexb 
 * @Date : 2018-08-14 09:57
 */
enum IDType {
    UUID, AUTO_INCREMENT, NO_KEY
}