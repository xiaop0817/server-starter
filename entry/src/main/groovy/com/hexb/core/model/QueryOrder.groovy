package com.hexb.core.model

import com.hexb.core.model.enums.OrderType

/**
 * @Package : com.hexb.core.model
 * @Author : hexb 
 * @Date : 2018-08-19 10:13
 */
class QueryOrder {

    String column
    OrderType orderType

    String toOrderSql() {
        "$column ${orderType.name()}"
    }
}
