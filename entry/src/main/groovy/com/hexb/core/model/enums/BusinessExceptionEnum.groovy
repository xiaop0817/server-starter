package com.hexb.core.model.enums

/**
 * @Package : com.hexb.core.exception
 * @Author : hexb 
 * @Date : 2018-08-3 13:04
 */
interface BusinessExceptionEnum {

    String getMessage()

    String getCode()

    String getKey()
}