package com.hexb.core.exception

/**
 * @Package : com.hexb.core.exception
 * @Author : hexb 
 * @Date : 2019-05-10 10:21
 */
interface I18Nable {
    Object[] getArgs()

    String getKey()

    String getMessage()
}
