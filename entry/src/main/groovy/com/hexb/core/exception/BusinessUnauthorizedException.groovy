package com.hexb.core.exception

import com.hexb.core.model.enums.BusinessExceptionEnum

/**
 * @Package : com.hexb.core.exception
 * @Author : hexb
 * @Date : 2018-08-30 14:32
 */
class BusinessUnauthorizedException extends RuntimeException implements I18Nable {
    String code
    Object[] args
    final String key

    BusinessUnauthorizedException(String message, String code) {
        super(message)
        this.code = code
    }

    BusinessUnauthorizedException(BusinessExceptionEnum e) {
        super(e.message)
        this.code = e.code
        this.key = e.key
    }

    BusinessUnauthorizedException(BusinessExceptionEnum e, Object... args) {
        this(e)
        this.args = args
    }
}
