package com.hexb.core.exception

import com.hexb.core.model.enums.BusinessExceptionEnum

/**
 * 业务异常
 */
class BusinessException extends RuntimeException implements I18Nable {

    String code
    Object[] args
    final String key

    BusinessException(String message, String code) {
        super(message)
        this.code = code
    }

    BusinessException(BusinessExceptionEnum e) {
        super(e.message)
        this.code = e.code
        this.key = e.key
    }

    BusinessException(BusinessExceptionEnum e, Object... args) {
        this(e)
        this.args = args
    }
}
