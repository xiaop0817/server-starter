package com.hexb.core.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb
 * @Date : 2018-08-7 17:28
 */
@Slf4j
public class JSONUtils {

    static public String toJSON(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Object to JSON Error", e);
        }
        return null;
    }

    static public <T> T toObject(String json, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("JSON to Object Error", e);
        }
        return null;
    }
}
