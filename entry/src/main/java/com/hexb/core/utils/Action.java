package com.hexb.core.utils;

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb
 * @Date : 2019-04-29 14:34
 */
@FunctionalInterface
public interface Action {
    void execute();
}