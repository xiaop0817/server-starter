package com.hexb.core.utils;

import com.hexb.core.exception.BusinessException;
import com.hexb.core.model.enums.BusinessExceptionEnum;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb
 * @Date : 2018-08-30 17:46
 */
public class Assert {

    public static void isTrue(boolean expression, String code, String message) {
        org.springframework.util.Assert.isTrue(expression, String.format("%s|%s", code, message));
    }

    public static void isFalse(boolean expression, String code, String message) {
        org.springframework.util.Assert.isTrue(!expression, String.format("%s|%s", code, message));
    }

    public static void isNull(Object object, String code, String message) {
        org.springframework.util.Assert.isNull(object, String.format("%s|%s", code, message));
    }

    public static void notNull(Object object, String code, String message) {
        org.springframework.util.Assert.notNull(object, String.format("%s|%s", code, message));
    }

    public static void notEmpty(Collection<?> collection, String code, String message) {
        org.springframework.util.Assert.notEmpty(collection, String.format("%s|%s", code, message));
    }

    public static void notEmpty(Object[] array, String code, String message) {
        org.springframework.util.Assert.notEmpty(array, String.format("%s|%s", code, message));
    }

    public static void notEmpty(String string, String code, String message) {
        if (!StringUtils.isEmpty(string)) {
            throw new BusinessException(message, code);
        }
    }

    public static void isEmpty(Object[] array, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (ObjectUtils.isEmpty(array)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isNull(Object object, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null == object) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notNull(Object object, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null != object) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isEmpty(String str, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (StringUtils.isEmpty(str)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isEmpty(Collection<?> coll, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (coll == null || coll.size() == 0) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notEmpty(String str, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (!StringUtils.isEmpty(str)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notEmpty(Collection<?> coll, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (coll != null && coll.size() > 0) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isTrue(Boolean bool, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (bool) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isFalse(Boolean bool, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (!bool) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isEqual(Object one, Object two, BusinessExceptionEnum exceptionEnum, Object... args) {
        boolean result = one != null && two != null && one.equals(two);
        if (result) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notEqual(Object one, Object two, BusinessExceptionEnum exceptionEnum, Object... args) {
        boolean result = one != null && two != null && !one.equals(two);
        if (result) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isContain(Collection<?> coll, Object item, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null != coll && coll.contains(item)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notContain(Collection<?> coll, Object item, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null != coll && !coll.contains(item)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void isContain(Collection<?> coll, Predicate<Object> filterMethod, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null != coll) {
            long count = coll.stream().filter(filterMethod).count();
            if (count > 0) {
                throw new BusinessException(exceptionEnum, args);
            }
        }
    }

    public static void notContain(Collection<?> coll, Predicate<Object> filterMethod, BusinessExceptionEnum exceptionEnum, Object... args) {
        if (null != coll) {
            long count = coll.stream().filter(filterMethod).count();
            if (count == 0) {
                throw new BusinessException(exceptionEnum, args);
            }
        }
    }

    /*
     * 断言是否是一个大陆手机号
     */
    public static void notMobileNoForCN(String phoneNo, BusinessExceptionEnum exceptionEnum, Object... args) {
        isEmpty(phoneNo, exceptionEnum, args);
        if (!phoneNo.matches(RegexUtils.MOBILE_CN)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void notMatch(String source, String regex, BusinessExceptionEnum exceptionEnum, Object... args) {
        isEmpty(source, exceptionEnum, args);
        if (!source.matches(regex)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }

    public static void match(String source, String regex, BusinessExceptionEnum exceptionEnum, Object... args) {
        isEmpty(source, exceptionEnum, args);
        if (source.matches(regex)) {
            throw new BusinessException(exceptionEnum, args);
        }
    }
}
