package com.hexb.core.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb
 * @Date : 2018-08-17 17:04
 */
@Slf4j
public class DateUtils {

    public static final String YEAR_MONTH = "yyyy-MM";
    public static final String YEAR_MONTH_DAY = "yyyy-MM-dd";
    public static final String YEAR_MONTH_DAY_HOUR_MINUTE = "yyyy-MM-dd HH:mm";
    public static final String YEAR_MONTH_DAY_HOUR_MINUTE_SECOND = "yyyy-MM-dd HH:mm:ss";
    public static final String FULL_TIME = "yyyy-MM-dd HH:mm:ss";


    private static final List<String> formarts = new ArrayList();

    static {
        formarts.add(YEAR_MONTH);
        formarts.add(YEAR_MONTH_DAY);
        formarts.add(YEAR_MONTH_DAY_HOUR_MINUTE);
        formarts.add(YEAR_MONTH_DAY_HOUR_MINUTE_SECOND);
    }

    static public Date getDayEnd(String d) {
        return getDayEnd(parseDate(d));
    }

    static public Date getDayBegin(String d) {
        return getDayBegin(parseDate(d));
    }

    static public Date getDayEnd(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        setCalTimeDayEnd(calendar);
        return calendar.getTime();
    }

    static public Date getDayBegin(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        setCalTimeDayBegin(calendar);
        return calendar.getTime();
    }

    static public void setCalTime(Calendar cal, int hour, int minute, int second, int milliSecond) {
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, milliSecond);
    }

    static public void setCalTimeDayEnd(Calendar cal) {
        setCalTime(cal, 23, 59, 59, 999);
    }

    static public void setCalTimeDayBegin(Calendar cal) {
        setCalTime(cal, 0, 0, 0, 0);
    }


    static public Date parseDate(String source) {
        String value = source.trim();
        if ("".equals(value)) {
            return null;
        }
        if (source.matches("^\\d{4}-\\d{1,2}$")) {
            return parseDate(source, formarts.get(0));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
            return parseDate(source, formarts.get(1));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
            return parseDate(source, formarts.get(2));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
            return parseDate(source, formarts.get(3));
        } else {
            throw new IllegalArgumentException("Invalid boolean value '" + source + "'");
        }
    }

    /**
     * 功能描述：格式化日期
     *
     * @param dateStr String 字符型日期
     * @param format  String 格式
     * @return Date 日期
     */
    static public Date parseDate(String dateStr, String format) {
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            date = dateFormat.parse(dateStr);
        } catch (Exception e) {
            log.error("String to Date convert Error!", e);
        }
        return date;
    }
}
