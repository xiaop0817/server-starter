package com.hexb.core.utils;

import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {

    private static Logger logger = Logger.getLogger(MD5Utils.class);

    public static String MD5(String string) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer();
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString().toUpperCase();
            logger.debug("MD5 : [ " + string + " ] = " + result);
        } catch (NoSuchAlgorithmException e) {
            logger.error("MD5加密出错!");
        }
        return result;
    }
}
