package com.hexb.core.utils;

/**
 * @Package : com.hexb.core.utils
 * @Author : hexb
 * @Date : 2018-08-14 14:24
 */
public class RegexUtils {

    @Deprecated
    public static final String IP4 = "^((?:(?:25[0-5]|2[0-4]\\d|(?:1\\d{2}|[1-9]?\\d))\\.){3}(?:25[0-5]|2[0-4]\\d|(?:1\\d{2}|[1-9]?\\d)))$";
    public static final String IPv4 = "^((?:(?:25[0-5]|2[0-4]\\d|(?:1\\d{2}|[1-9]?\\d))\\.){3}(?:25[0-5]|2[0-4]\\d|(?:1\\d{2}|[1-9]?\\d)))$";
    public static final String IPv6 = "^([\\da-fA-F]{1,4}:){7}[\\da-fA-F]{1,4}$";
    public static final String IP = String.format("%s|%s", IPv4, IPv6);
    public static final String MAC = "^[0-9a-fA-F]{2}([:|-][0-9a-fA-F]{2}){5}$";
    public static final String MOBILE_CN = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    public static final String PASSWORD_NOT_BLANK_NOT_ZH_4_20 = "^[^(\\s|\\u4e00-\\u9fa5)]{4,20}$";
    /**
     * 地址码判定不够精确
     * 1.我国并不存在16，26开头的地区，却可通过验证
     * 2.日期判定不够精确。例:19490231也可通过验证，而2月并不存在31日
     * 3.校验码是由17位本体码计算得出，方案1并未校验此码
     */
    public static final String ID_CARD_NO = "^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
}
