package com.hexb.core.utils;


import com.hexb.core.model.ValueEnum;

public class ValueEnumUtils {

    static public <E extends ValueEnum> E getEnumByValue(Class<E> clazz, int value) {
        E[] enums = clazz.getEnumConstants();
        for (E e : enums) {
            if (e.getValue() == value) {
                return e;
            }
        }
        return null;
    }
}
