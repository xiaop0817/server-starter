package com.hexb.core.enums;

/**
 * @Package : com.hexb.core.enums
 * @Author : hexb
 * @Date : 2018-08-18 10:52
 */
public enum AscColors {

    BG_BLACK("\033[40m"),
    BG_RED("\033[41m"),
    BG_GREEN("\033[42m"),
    BG_YELLOW("\033[43m"),
    BG_BLUE("\033[44m"),
    BG_VIOLET("\033[45m"),
    BG_DARK_GREEN("\033[46m"),
    BG_WHITE("\033[47m"),

    BLACK("\033[30m"),
    RED("\033[31m"),
    GREEN("\033[32m"),
    YELLOW("\033[33m"),
    BLUE("\033[34m"),
    VIOLET("\033[35m"),
    DARK_GREEN("\033[36m"),
    WHITE("\033[37m"),

    LIGHT("\033[1m"),
    END("\033[0m");


    private String value;

    AscColors(String value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return value;
    }
}
