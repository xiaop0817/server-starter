package com.hexb.core.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hexb.core.annotation.InsertIgnore;
import com.hexb.core.annotation.UpdateIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Package : com.hexb.core.model
 * @Author : hexb
 * @Date : 2018-08-15 10:12
 */
@Data
public class Model implements Serializable {

    private static final long serialVersionUID = -6457192105763899819L;

    @InsertIgnore
    @UpdateIgnore
    private String id;

    @ApiModelProperty(hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @InsertIgnore
    @UpdateIgnore
    private Date createTime;

    @ApiModelProperty(hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @InsertIgnore
    private Date updateTime;

}
