package com.hexb.core.model;

import com.hexb.core.model.enums.BusinessExceptionEnum;

/**
 * @Package : com.hexb.core.model
 * @Author : hexb
 * @Date : 2019-03-29 17:42
 */
public enum BaseErrorCodes implements BusinessExceptionEnum {

    PARAMETER_IS_NULL("参数为空", "parameter_is_null");

    private String message;
    private String code;
    private String key;

    BaseErrorCodes(String message, String code, String key) {
        this.message = message;
        this.code = code;
        this.key = key;
    }

    BaseErrorCodes(String message, String code) {
        this.message = message;
        this.code = code;
        this.key = code;
    }

    BaseErrorCodes(String code) {
        this.message = code;
        this.code = code;
        this.key = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }
}
