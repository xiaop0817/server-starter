package com.hexb.core.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.beanutils.BeanMap;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Package : com.hexb.core.model
 * @Author : hexb
 * @Date : 2018-08-15 10:24
 */
@Data
public class QueryParameter {

    @ApiModelProperty(value = "排序", example = "name ASC,createTime DESC")
    private String orderBy;

    public Map map() {
        return new BeanMap(this);
    }
}
