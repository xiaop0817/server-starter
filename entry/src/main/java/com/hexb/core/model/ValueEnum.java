package com.hexb.core.model;


import com.fasterxml.jackson.annotation.JsonValue;
import com.hexb.core.exception.InvalidValueOfEnumException;
import com.hexb.core.utils.ValueEnumUtils;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Alias("valueEnum")
public interface ValueEnum<E extends Enum<E>> extends Serializable {

    static <E extends Enum<E>> E byValue(Class<? extends ValueEnum> clazz, int value) {
        E enumByValue = (E) ValueEnumUtils.getEnumByValue(clazz, value);
        if (enumByValue == null) {
            throw new InvalidValueOfEnumException();
        }
        return enumByValue;
    }

    @JsonValue
    int getValue();

    String getDescription();
}
