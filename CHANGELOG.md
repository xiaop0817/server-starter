#### 2018-07-31
* 添加mapper#update,mapper#insert
* 添加shiro支持

#### 2018-07-18
* 添加默认typeHandler `[ EnumOrdinalTypeHandler , EnumValueTypeHandler ]`

#### 2018-07-17
* swagger配置
* Druid数据源配置
* 枚举扫描器配置
* 跨域filter配置
* 全局异常配置
* 接口返回数据包装器配置
* mybatis基础mapper ``[select,selectOne,count,insert,insertBatch]``

#### 2018-08-20
* AbstractBaseService 公共分页方法(pageCommonAnyType)

#### 2018-12-07
* 添加多数据源
```$xslt
@Configuration
@EnableConfigurationProperties({PrimaryProperties.class, SecondaryProperties.class, DruidProperties.class})
public class MConfig {

    @Autowired
    private PrimaryProperties primaryProperties;

    @Autowired
    private SecondaryProperties secondaryProperties;

    @Autowired
    private DruidProperties druidProperties;

    public DataSource primary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(primaryProperties).build();
    }

    public DataSource secondary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(secondaryProperties).build();
    }

    //注意：primary()不能声明为Bean
    @Bean
    public MultipleDataSource getMultipleDataSource() {
        MultipleDataSource ds = new MultipleDataSource();
        ds.registor("primaryDS", primary());
        ds.registor("secondaryDS", secondary());
        ds.setDefaultKey("primaryDS");
        return ds;
    }
}
```
#### 2018-12-07
* ObjectUtils.saveCall工具,类似其它新语言 (.?) 操作符方法

#### 2019-02-26
* 添加FeignErrorDecoder 配置项:project.feignErrorCatch默认启用
* 修复自动包装Response时影响FeignClient RequestBody请求同时被包装问题
* 添加Assert.match/Assert.notMatch


TODO: 如果mybatis配置了 map-underscore-to-camel-case: true 则对应sql要遵守此规则