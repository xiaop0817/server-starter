package com.hexb.core.aop;

import com.hexb.core.service.AbstractBaseService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Package : com.hexb.core.aop
 * @Author : hexb
 * @Date : 2018-08-30 17:38
 */

@Aspect
@Component
public class BaseServiceAspect {

    @Pointcut("execution(* com.hexb.core.service.AbstractBaseService.getPrimaryMapper())")
    public void checker() {
    }

    @Before("checker()")
    public void checkMapper(JoinPoint joinPoint) {
        ((AbstractBaseService)joinPoint.getTarget()).checkDefaultMapper();
    }

}