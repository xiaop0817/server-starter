package com.hexb.core.web;

import java.lang.reflect.Type;

/**
 * @Package : com.hexb.core.web
 * @Author : hexb
 * @Date : 2018-08-26 14:48
 */
public interface ResponseWrapFilter {

    Boolean exclude(Object object, Type type);
}
