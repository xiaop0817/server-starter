package com.hexb.core.web.controller;

import com.hexb.core.utils.VersionUtils;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package : com.hexb.core.web.controller
 * @Author : hexb
 * @Date : 2019-08-9 16:31
 */
@RestController
@RequestMapping("/project-version")
@Api(value = "工程版本", description = "工程中版本定义")
@ConditionalOnProperty(value = "project.showVersion", havingValue = "true", matchIfMissing = true)
public class VersionController {


    @Value("${project.buildVersionKey:App-Version}")
    private String buildVersionKey;

    @Value("${project.sourceVersion:1.0.0}")
    private String sourceVersion;

    @Value("${project.databaseVersion:1.0.0}")
    private String databaseVersion;

    @GetMapping
    public ProjectVersion get() {
        return ProjectVersion.builder().buildVersion(
                VersionUtils.manifestValue(buildVersionKey))
                .sourceVersion(sourceVersion)
                .databaseVersion(databaseVersion).build();
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class ProjectVersion {
        /**
         * 构建版本号
         */
        private String buildVersion;

        /**
         * 源码版本号
         */
        private String sourceVersion;

        /**
         * 数据库版本号
         */
        private String databaseVersion;
    }
}
