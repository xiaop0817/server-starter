package com.hexb.core.web.controller;

import com.hexb.core.scanner.ErrorCodeScanner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author hexb
 * @create 2017-07-10 下午5:55
 **/

@RestController
@ConditionalOnBean(name = "errorCodeScanner")
@RequestMapping("/project-error-code")
@Api(value = "错误码", description = "工程中使用的错误码定义")
public class ErrorCodeController {

    @Autowired
    @Qualifier(value = "errorCodeScanner")
    private ErrorCodeScanner errorCodeScanner;

    @ApiOperation(value = "错误码列表", httpMethod = "GET")
    @GetMapping
    public List<Map<String, Object>> list() {
        return errorCodeScanner.getList();
    }
}
