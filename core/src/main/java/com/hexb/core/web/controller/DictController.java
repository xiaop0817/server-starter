package com.hexb.core.web.controller;

import com.hexb.core.scanner.DictionaryScanner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author hexb
 * @create 2017-07-10 下午5:55
 **/

@RestController
@ConditionalOnBean(name = "dictScanner")
@RequestMapping("/project-dictionaries")
@Api(value = "系统枚举值", description = "项目中使用到的枚举值定义")
public class DictController {


    @Autowired
    @Qualifier(value = "dictScanner")
    private DictionaryScanner dictScanner;

    @ApiOperation(value = "系统枚举值列表", httpMethod = "GET")
    @GetMapping
    public List<Map<String, Object>> list() {
        return dictScanner.getList();
    }
}
