package com.hexb.core.mapper;


import com.hexb.core.mybatis.entry.BatchUpdateParam;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface BaseMapper<PK, T> extends Serializable {

    List<T> selectByMap(Map param);

    List<T> select(T t);

    T selectOne(PK id);

    T selectOneByEntry(T t);

    int insert(T t);

    int insertBatch(List<T> t);

    int update(T t);

    int updateByMap(Map<String, Object> map);

    int updateBatch(BatchUpdateParam<PK, T> param);

    int updateBatchByMap(BatchUpdateParam<PK, Map<String, Object>> param);

    int delete(PK id);

    int deleteBatch(PK[] ids);

    int count(Map param);

    int countByEntry(T t);

    @Deprecated
    /**
     * 方法名错误,应该为selectWithoutPK
     * @See BaseMapper#selectWithoutPK
     * */
    List<T> selectWithPK(T t);

    /**
     * 查询列表,但排除id为当前参数id的记录,主要用于更新时判断重复
     */
    List<T> selectWithoutPK(T t);

}