package com.hexb.core.service;

import com.github.pagehelper.PageHelper;
import com.hexb.core.common.Page;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @Package : com.hexb.core.service
 * @Author : hexb
 * @Date : 2019-08-15 11:35
 */
public class Pagination {

    static public <S, E> Page<E> pageCommonAnyType(S param, int pageNum, int pageSize, Function<S, List<E>> func) {
        com.github.pagehelper.Page page = PageHelper.startPage(pageNum, pageSize);
        List<E> dataList = func.apply(param);
        Page<E> pageInfo = createPage(dataList, page);
        pageInfo.setPageSize(pageSize);
        return pageInfo;
    }

    static public <E> Page<E> pageCommonAnyType(int pageNum, int pageSize, Supplier<List<E>> supplier) {
        com.github.pagehelper.Page page = PageHelper.startPage(pageNum, pageSize);
        List<E> dataList = supplier.get();
        Page<E> pageInfo = createPage(dataList, page);
        pageInfo.setPageSize(pageSize);
        return pageInfo;
    }

    private static <E> Page<E> createPage(List<E> dataList, com.github.pagehelper.Page page) {
        Page<E> pageInfo = new Page<>(dataList);
        pageInfo.setPageNum(page.getPageNum());
        pageInfo.setTotal(page.getTotal());
        return pageInfo;
    }
}
