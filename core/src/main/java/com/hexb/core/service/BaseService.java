package com.hexb.core.service;


import com.hexb.core.common.Page;
import com.hexb.core.model.QueryParameter;

import java.util.List;
import java.util.Map;

public interface BaseService<PK, T, Q extends QueryParameter> {

    Page<T> page(Q param, int pageNum, int pageSize);

    Page<T> page(Map param, int pageNum, int pageSize);

    Page<T> page(T t, int pageNum, int pageSize);


    List<T> select(Q param);

    List<T> select(Map param);

    List<T> select(T t);

    List<T> selectWithoutPk(T t);

    T selectOne(PK id);

    T selectOneByEntry(T t);


    int insert(T t);

    int insertBatch(List<T> list);


    int update(T t);

    int updateByMap(Map<String, Object> param);

    int updateBatch(PK[] ids, T params);

    int updateBatchByMap(PK[] ids, Map<String, Object> params);

    int delete(PK id);

    int deleteBatch(PK[] ids);


    long count(T t);

    long count(Q param);

    long count(Map param);

}
