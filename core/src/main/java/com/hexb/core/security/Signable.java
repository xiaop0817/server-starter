package com.hexb.core.security;

import com.hexb.core.utils.ObjectHelper;
import com.hexb.core.utils.RSAUtils;

/**
 * @Package : com.hexb.core.security
 * @Author : hexb
 * @Date : 2018-08-17 17:11
 */
public interface Signable {

    /**
     * 生成当前对象原始待签字符串
     */
    default String original() {
        String propertyString = ObjectHelper.getSortedPropertiesString(this, excludeFields());
        return propertyString;
    }

    /**
     * 生成当前对象签名字符串
     */
    default String sign(String privateKey) throws Exception {
        String sign = RSAUtils.sign(original(), privateKey);
        return sign;
    }

    /**
     * 验证签名字符串
     */
    default Boolean verify(String publicKey, String signStr) throws Exception {
        return RSAUtils.verify(original(), publicKey, signStr);
    }

    /**
     * 要排除的字段名
     */
    String[] excludeFields();
}
