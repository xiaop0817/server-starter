package com.hexb.core.jdbc;

import java.lang.annotation.*;

/**
 * @Package : com.hexb.core.service
 * @Author : hexb
 * @Date : 2018-08-7 11:15
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TargetDataSource {

    String value();
}
