package com.hexb.core.jdbc;

import com.hexb.core.enums.AscColors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Package : com.hexb.core.service
 * @Author : hexb
 * @Date : 2018-08-7 16:16
 */
@Slf4j
public class MultipleDataSource {

    private Map<Object, Object> maps = new HashMap<>();

    private Object defaultKey;

    public MultipleDataSource() {
    }

    public Map<Object, Object> getDataSources() {
        return maps;
    }

    public void register(String name, DataSource dataSource) {
        maps.put(name, dataSource);
    }

    public void setDefaultKey(Object defaultKey) {
        this.defaultKey = defaultKey;
    }

    public Object getDefault() {
        if (null == this.defaultKey) {
            log.error(String.format("%s====>%s Default Data Source Key Not Set!  %s", AscColors.YELLOW, AscColors.VIOLET, AscColors.END));
        }
        return maps.get(this.defaultKey);
    }
}
