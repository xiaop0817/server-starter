package com.hexb.core.jdbc;

/**
 * @Package : com.hexb.core.service
 * @Author : hexb
 * @Date : 2018-08-7 10:54
 * 多数据源上下文
 */
public class DynamicDataSourceContextHolder {

    private static ThreadLocal<String> dataSourceNames = new ThreadLocal<>();

    static public void setDataSourceName(String name) {
        dataSourceNames.set(name);
    }

    static public String getDataSourceName() {
        return dataSourceNames.get();
    }

    static public void clearDataSourceName() {
        dataSourceNames.remove();
    }
}
