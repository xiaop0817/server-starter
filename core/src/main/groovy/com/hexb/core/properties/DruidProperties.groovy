package com.hexb.core.properties


import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * @author hexb
 * @create 2017-11-01 15:59
 * */
@ConfigurationProperties("druid")
class DruidProperties {

    int initialSize = 10

    int minIdle = 10

    int maxActive = 100

    int maxWait = 60000

    int timeBetweenEvictionRunsMillis = 60000

    int minEvictableIdleTimeMillis = 300000

    String validationQuery = 'select 1 from dual'

    boolean testWhileIdle = true

    boolean testOnBorrow = false

    boolean testOnReturn = false

    boolean poolPreparedStatements = true

    int maxPoolPreparedStatementPerConnectionSize = 20

    String filters = 'stat,wall,log4j,config'

    String connectionProperties = 'config.decrypt=true;druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000'

    int removeAbandonedTimeout = 180

}
