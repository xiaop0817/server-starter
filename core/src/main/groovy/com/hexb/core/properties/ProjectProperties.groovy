package com.hexb.core.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 * @Package : com.hexb.core.config
 * @Author : hexb 
 * @Date : 2018-08-12 15:49
 */
@ConfigurationProperties(prefix = 'project')
class ProjectProperties {

    String dictScannerPackages
    String errorCodeScannerPackages
    String mapperPackages
    //实体名映射表名是否自动驼峰转下划线
    Boolean entryToTableCamelToUnderscore = true
    //实体字段映射列是否自动驼峰转下划线
    Boolean fieldToColumnCamelToUnderscore = false

    Boolean dictScanner
    Boolean errorCodeScanner
    Boolean exceptionAdvice
    Boolean crossDomain
    Boolean responseWrap
    Boolean swagger
    Boolean useBaseMapper
    String wrapExclude

}


