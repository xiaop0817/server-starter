package com.hexb.core.properties

import com.hexb.core.common.IDataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * @author hexb
 * @create 2017-11-01 15:59
 *
 * 1.@EnableConfigurationProperties 开启
 * 2.被注解的类字段需要setter
 * 3.@ConfigurationProperties注解去除了location属性，idea会报：Spring Boot Annotation processor not found in classpath
 * 4.如果不使用groovy，可以使用<B>Lombok</B>插件来编写类似groovy的简约java类
 * */
@ConfigurationProperties('spring.datasource')
class DataSourceProperties implements IDataSourceProperties {

    String url

    String username

    String password

    String driverClassName

}
