package com.hexb.core.properties

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * @Package : com.hexb.core.properties
 * @Author : hexb 
 * @Date : 2018-08-13 11:46
 */
@ConfigurationProperties(prefix = 'swagger')
class SwaggerProperties {

    String title = 'Title'
    String description = 'Description'
    String version = '1.0.0'
    Boolean oathuHeader = true
}
