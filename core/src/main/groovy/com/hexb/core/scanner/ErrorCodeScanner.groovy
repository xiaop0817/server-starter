package com.hexb.core.scanner


import com.hexb.core.annotation.ErrorCode
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.core.io.support.ResourcePatternResolver
import org.springframework.core.type.classreading.CachingMetadataReaderFactory
import org.springframework.core.type.classreading.MetadataReader
import org.springframework.core.type.classreading.MetadataReaderFactory
import org.springframework.util.ClassUtils
import org.springframework.util.StringUtils
import org.springframework.util.SystemPropertyUtils

import java.lang.reflect.Method


/**
 * @author hexb
 * @create 2017-07-07 下午4:38
 * */

class ErrorCodeScanner {

    static final String DEFAULT_RESOURCE_PATTERN = "**/*.class"
    final List<Map<String, Object>> list = []
    Logger logger = LoggerFactory.getLogger(ErrorCodeScanner.class)
    private String scanPackages = ""

    ErrorCodeScanner(String scanPackages) {
        this.scanPackages = scanPackages
        scanner()
    }

    def scanner() {
        logger.info("\u001B[34m====>\u001B[0m begin to scan \u001B[31m[ErrorCodes]\u001B[0m in $scanPackages")
        String[] scanPackageArr = scanPackages.split(",")
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver()
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver)
        for (String basePackage : scanPackageArr) {
            if (StringUtils.isEmpty(basePackage)) {
                continue
            }
            logger.info("\u001B[34m====>\u001B[0m scanning [ErrorCodes] ${basePackage}")
            String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                    ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage)) + "/" + DEFAULT_RESOURCE_PATTERN
            try {
                Resource[] resources = resourcePatternResolver.getResources(packageSearchPath)
                for (Resource resource : resources) {
                    loadClassMethod(metadataReaderFactory, resource)
                }
            } catch (Exception e) {
                logger.error("ErrorCodes scan error!", e)
            }

        }
        logger.info("\u001B[34m====>\u001B[0m errorCodes scan finished，has \u001B[31m${list.size()}\u001B[0m items")
    }

    private void loadClassMethod(MetadataReaderFactory metadataReaderFactory, Resource resource) throws IOException {

        try {
            if (resource.isReadable()) {
                MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource)
                if (metadataReader != null) {
                    String[] types = metadataReader.annotationMetadata.annotationTypes
                    if (types.contains(ErrorCode.class.getName())) {
                        Class<?> clazz = Class.forName(metadataReader.getClassMetadata().getClassName())
                        ErrorCode dict = clazz.getAnnotation(ErrorCode.class)
                        Map item = [
                                module     : dict.module(),
                                typeName   : clazz.simpleName,
                                name       : dict.value(),
                                description: dict.description(),
                        ]
                        item.items = []
                        def constants = clazz.enumConstants
                        def map
                        constants.each {
                            map = [:]
                            map << [name: clazz.getMethod("name").invoke(it)]
                            Method[] methods = it.class.declaredMethods
                            methods.each { method ->
                                if (method.getName() == "getCode") {
                                    map << [code: clazz.getMethod("getCode").invoke(it)]
                                }
                                if (method.getName() == "getMessage") {
                                    map << [message: clazz.getMethod("getMessage").invoke(it)]
                                }
                            }
                            item.items << map
                        }
                        list << item
                    }
                }
            }
        } catch (Exception e) {
            logger.error("ErrorCodes scan error!", e)
        }
    }
}
