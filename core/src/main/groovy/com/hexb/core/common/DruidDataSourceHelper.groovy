package com.hexb.core.common

import com.hexb.core.properties.DataSourceProperties
import com.hexb.core.properties.DruidProperties

import javax.sql.DataSource

/**
 * @Package : com.hexb.core.common
 * @Author : hexb 
 * @Date : 2018-08-7 15:57
 */
class DruidDataSourceHelper {

    static bindProperties(DataSource source, DruidProperties druidProperties) {
        source['initialSize'] = druidProperties.initialSize
        source['minIdle'] = druidProperties.minIdle
        source['maxActive'] = druidProperties.maxActive
        source['maxWait'] = druidProperties.maxWait
        source['timeBetweenEvictionRunsMillis'] = druidProperties.timeBetweenEvictionRunsMillis
        source['minEvictableIdleTimeMillis'] = druidProperties.minEvictableIdleTimeMillis
        source['validationQuery'] = druidProperties.validationQuery
        source['testWhileIdle'] = druidProperties.testWhileIdle
        source['testOnBorrow'] = druidProperties.testOnBorrow
        source['testOnReturn'] = druidProperties.testOnReturn
        source['maxPoolPreparedStatementPerConnectionSize'] = druidProperties.maxPoolPreparedStatementPerConnectionSize
        source['filters'] = druidProperties.filters
        source['connectionProperties'] = druidProperties.connectionProperties
        source['removeAbandonedTimeout'] = druidProperties.removeAbandonedTimeout
        source['poolPreparedStatements'] = druidProperties.poolPreparedStatements
    }

    static bindJdbc(DataSource source, IDataSourceProperties dataSourceProperties) {
        source['url'] = dataSourceProperties.url
        source['username'] = dataSourceProperties.username
        source['password'] = dataSourceProperties.password
        source['driverClassName'] = dataSourceProperties.driverClassName
    }
}
