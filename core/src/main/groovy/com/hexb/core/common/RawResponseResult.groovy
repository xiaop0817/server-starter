package com.hexb.core.common

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonRawValue

/**
 * 返回原始数据类型
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class RawResponseResult {

    /**
     * 原始json字符串
     * */
    @JsonRawValue
    String data

}
