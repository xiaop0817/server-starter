package com.hexb.core.common
/**
 * restful接口数据返回结构
 * @field data
 * @field code
 * @field message
 * */
class ResponseResultBuilder<T> {

    private ResponseResult<T> resResult

    ResponseResultBuilder() {
        resResult = new ResponseResult<>()
    }

    ResponseResultBuilder(ResponseResult<T> rr) {
        this.resResult = rr
    }

    def data(T data) {
        resResult.data = data
        this
    }

    def status(int stats) {
        resResult.status = stats
        this
    }

    def code(String code) {
        resResult.code = code
        this
    }

    def message(String message) {
        resResult.message = message
        this
    }

    ResponseResult<T> build() {
        resResult
    }

    static ResponseResultBuilder<T> init() {
        new ResponseResultBuilder<T>()
    }
}
