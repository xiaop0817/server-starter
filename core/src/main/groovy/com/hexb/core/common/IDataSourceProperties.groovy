package com.hexb.core.common

/**
 * @Package : com.hexb.core.common
 * @Author : hexb 
 * @Date : 2018-08-7 16:55
 */
interface IDataSourceProperties {

    String getUrl();

    String getUsername();

    String getPassword();

    String getDriverClassName();
}