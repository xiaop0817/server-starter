package com.hexb.core.common

class Page<E> implements Serializable {

    private static final long serialVersionUID = 8014333126676942851L

    int pageNum = 1
    long total = 0
    int pageSize = 10
    List<E> pageData = new ArrayList<>()

    Page() {
    }

    Page(List<E> pageData, int pageNum = 1, long total = 0, int pageSize = 10) {
        this.pageNum = pageNum
        this.total = total
        this.pageData = pageData
        this.pageSize = pageSize
    }
}
