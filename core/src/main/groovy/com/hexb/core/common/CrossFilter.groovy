package com.hexb.core.common

import org.springframework.web.filter.OncePerRequestFilter

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * @Package : com.hexb.core.common
 * @Author : hexb 
 * @Date : 2018-08-20 16:52
 */
class CrossFilter extends OncePerRequestFilter{

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        response.addHeader('Access-Control-Allow-Origin', request.getHeader('origin'))
        response.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
        response.addHeader('Access-Control-Allow-Headers',
                'origin, content-type, exclude, x-requested-with, sid, mycustom, smuser,Authorization')
        response.addHeader('Access-Control-Allow-Credentials', 'true')
        filterChain.doFilter(request, response)
    }
}
