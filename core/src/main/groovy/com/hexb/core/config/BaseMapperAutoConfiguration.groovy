package com.hexb.core.config

import com.hexb.core.mybatis.BaseMapperConfiguration
import com.hexb.core.mybatis.MapStatementBuilder
import com.hexb.core.properties.ProjectProperties
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * @Package : com.hexb.core.config
 * @Author : hexb 
 * @Date : 2018-08-13 14:11
 */
@ConditionalOnProperty(value = 'project.useBaseMapper', havingValue = 'true')
@Configuration
@EnableConfigurationProperties(ProjectProperties.class)
class BaseMapperAutoConfiguration {

    @Autowired
    ProjectProperties projectProperties

    @Bean
    ConfigurationCustomizer configurationCustomizer() {
        new BaseMapperConfiguration(projectProperties.mapperPackages, mapStatementBuilder())
    }

    @Bean
    MapStatementBuilder mapStatementBuilder() {
        new MapStatementBuilder()
    }
}
