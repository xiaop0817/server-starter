package com.hexb.core.config

import com.hexb.core.web.IntToValueEnumConverterFactory
import com.hexb.core.web.ResponseResultMappingJackson2HttpMessageConverter
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.format.FormatterRegistry
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport

/**
 * @author hexb* @create 2017-11-02 14:32
 * */
@Configuration
@ConditionalOnProperty(value = 'project.defaultWebConfig', havingValue = 'true', matchIfMissing = true)
class WebConfiguration extends WebMvcConfigurationSupport {

    @Value('${project.responseWrap:false}')
    private Boolean responseWrap = false

    @Value('${project.wrapExclude:}')
    private String wrapExclude = ""

    @Value('${project.index:/index.html}')
    private String index

    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        if (responseWrap) {
            converters.add(mappingJackson2HttpMessageConverter())
        }
        super.configureMessageConverters(converters)
    }

    @Override
    protected void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new IntToValueEnumConverterFactory())
        super.addFormatters(registry)
    }

    @Bean
    @ConditionalOnProperty(value = 'project.responseWrap', havingValue = 'true')
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        ResponseResultMappingJackson2HttpMessageConverter messageConverter = new ResponseResultMappingJackson2HttpMessageConverter(wrapExclude)
        List<MediaType> mediaTypes = new ArrayList<>()
        mediaTypes.add(MediaType.APPLICATION_JSON_UTF8)
        mediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED)
        messageConverter.setSupportedMediaTypes(mediaTypes)
        messageConverter
    }

    @Override
    void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/")
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/")
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/")
        super.addResourceHandlers(registry)
    }

    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:${index}")
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE)
        super.addViewControllers(registry)
    }
}
