package com.hexb.core.config

import com.hexb.core.properties.DataSourceProperties
import com.hexb.core.properties.DruidProperties
import com.hexb.core.properties.ProjectProperties
import com.hexb.core.utils.DruidDataSourceBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

import javax.sql.DataSource

/**
 * @Package : com.hexb.core.config
 * @Author : hexb 
 * @Date : 2018-08-13 11:02
 */
@Configuration
@EnableConfigurationProperties([DataSourceProperties.class, DruidProperties.class])
class DruidAutoConfiguration {

    @Autowired
    DataSourceProperties dataSourceProperties

    @Autowired
    DruidProperties druidProperties

    /*
    * 单数据源情况下使用
    * */

    @Bean
    @FlywayDataSource
    @ConditionalOnClass(name = ['com.alibaba.druid.pool.DruidDataSource'])
    @ConditionalOnProperty(value = "project.multipleDataSource", havingValue = "false", matchIfMissing = true)
    DataSource dataSource() {

        return new DruidDataSourceBuilder()
                .druid(druidProperties)
                .jdbc(dataSourceProperties)
                .build()

    }

}
