package com.hexb.core.config

import com.hexb.core.properties.ProjectProperties
import com.hexb.core.scanner.DictionaryScanner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * @author hexb
 * @create 2017-11-02 11:26
 * */

@Configuration
@EnableConfigurationProperties([ProjectProperties.class])
class DictScannerConfiguration {

    @Autowired
    ProjectProperties properties

    @Bean(name = "dictScanner")
    @ConditionalOnProperty(value = 'project.dictScanner', havingValue = 'true')
    DictionaryScanner dictScanner() {
        new DictionaryScanner(properties.dictScannerPackages)
    }
}
