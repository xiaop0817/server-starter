package com.hexb.core.mybatis

import com.hexb.core.mybatis.entry.SqlEntry
import org.apache.ibatis.mapping.MappedStatement
import org.apache.ibatis.session.Configuration

/**
 * @Package : com.hexb.core.mybatis
 * @Author : hexb 
 * @Date : 2018-08-16 10:23
 */
class MapStatementBuilder {


    List<MappedStatement> create(SqlEntry entry, Configuration configuration) {
        def list = []
        if (entry) {
            list << SQLSelectBuilder.select(entry, configuration)
            list << SQLSelectBuilder.selectByMap(entry, configuration)
            list << SQLSelectBuilder.selectWithPK(entry, configuration)
            list << SQLSelectBuilder.selectWithoutPK(entry, configuration)
            list << SQLSelectBuilder.selectOne(entry, configuration)
            list << SQLSelectBuilder.count(entry, configuration)
            list << SQLSelectBuilder.countByEntry(entry, configuration)
            list << SQLSelectBuilder.selectOneByEntry(entry, configuration)
            list << SQLInsertBuilder.insert(entry, configuration)
            list << SQLInsertBuilder.insertBatch(entry, configuration)
            list << SQLUpdateBuilder.update(entry, configuration)
            list << SQLUpdateBuilder.updateByMap(entry, configuration)
            list << SQLUpdateBuilder.updateBatch(entry, configuration)
            list << SQLUpdateBuilder.updateBatchByMap(entry, configuration)
            list << SQLDeleteBuilder.delete(entry, configuration)
            list << SQLDeleteBuilder.deleteBatch(entry, configuration)
        }
        list
    }

}
