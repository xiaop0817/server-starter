package com.hexb.core.mybatis.entry

import com.hexb.core.annotation.Id
import com.hexb.core.model.enums.IDType
import org.apache.ibatis.executor.keygen.KeyGenerator

/**
 * @Package : com.hexb.core.mybatis.entry
 * @Author : hexb 
 * @Date : 2018-08-16 15:49
 */
class IdEntry extends ColumnEntry {

    Class<?> idClass

    IDType idType
}
