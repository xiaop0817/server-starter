package com.hexb.core.mybatis.entry

/**
 * @Package : com.hexb.core.mybatis.entry
 * @Author : hexb 
 * @Date : 2018-08-16 17:44
 */
class ColumnEntry {

    //实体字段
    String property
    //表列
    String column
    Boolean insertIgnore = false
    Boolean updateIgnore = false

    ColumnEntry() {
    }

    ColumnEntry(String property, String column) {
        this.property = property
        this.column = column
    }

    ColumnEntry(String property, String column, Boolean insertIgnore, Boolean updateIgnore) {
        this.property = property
        this.column = column
        this.insertIgnore = insertIgnore
        this.updateIgnore = updateIgnore
    }
}
