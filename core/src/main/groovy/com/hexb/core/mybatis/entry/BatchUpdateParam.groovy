package com.hexb.core.mybatis.entry

/**
 * @Package : com.hexb.core.mybatis.entry
 * @Author : hexb 
 * @Date : 2018-08-19 09:46
 */
class BatchUpdateParam<PK, T> {

    PK[] ids
    T entry
}
