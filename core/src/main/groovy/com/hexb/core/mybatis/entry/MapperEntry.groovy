package com.hexb.core.mybatis.entry

import groovy.transform.ToString

/**
 * @Package : com.hexb.mt.model
 * @Author : hexb 
 * @Date : 2018-08-11 14:51
 */
@ToString
class MapperEntry {

    String mapperName
    String pkType
    String entryType

}
