package com.hexb.core.mybatis.entry

import org.apache.ibatis.mapping.ResultMapping

/**
 * @Package : com.hexb.mt.model
 * @Author : hexb 
 * @Date : 2018-08-11 15:31
 */
class SqlEntry {

    //mapper name
    String namespace

    String tableName

    String orderBy

    IdEntry idEntry = new IdEntry(column: 'id', property: 'id', idClass: String.class)

    Class<?> entryClass

    List<ResultMapping> resultMappings = []

    List<ColumnEntry> propertyMapping = []

    def getIdClass() {
        idEntry ? idEntry.idClass : String.class
    }
}
