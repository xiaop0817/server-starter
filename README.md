
## spring-boot-project-starter
## version 1.0.0
1. 提供SpringBoot / SpringCloud项目基本依赖
2. 提供基础功能，如数据库访问、API接口生成、异常处理、restful接口格式自动包装/拆包

### 使用

```
 ext{
    springBootVersion = '1.5.14.RELEASE'
 }
 
 dependencyManagement {
        imports {
            mavenBom("org.springframework.boot:spring-boot-dependencies:${springBootVersion}")
        }
    }

//引入依赖
dependencies {
        compile group: 'com.feitian.core', name: 'feitian-spring-boot-starter', version: '1.0.0-SNAPSHOT'
    }

```

##### 依赖版本
- 基于springBoot **1.5.14**


##### BaseMapper
- 参考开源项目：mybatis plus
- 提供单表CRUD操作
- **注意：** 提供自带方法目前不允许被同名方法覆盖，即不能在mapper.xml中出现已有id节点
- **注意：** 使用UUID为主键时，批量插入不能返回插入对象ID
```
    List<T> selectByMap(Map param);

    List<T> select(T t);

    T selectOne(PK id);

    int insert(T t);

    int insertBatch(List<T> t);

    int update(T t);

    int updateByMap(Map<String, Object> map);

    int updateBatch(BatchUpdateParam<PK, T> param);

    int updateBatchByMap(BatchUpdateParam<PK, Map<String, Object>> param);

    int delete(PK id);

    int deleteBatch(PK[] ids);

    int count(Map param);
    
```

##### Entry Annotations 用于BaseMapper自动方法生成时的注解

* @Id 指定字段为主键列，以及主键生成方式，目前支持：uuid和自增两种
* @Table 当实体类与表名不一致时使用指定表名
* @Column 映射字段与表列
* @InsertIgnore insert时忽略此字段
* @UpdateIgnore update时忽略此字段
* 使用baseMapper insert/update时对象中的复杂对象会被自动忽略


##### shiro
* 需要自行实现Realm,否则默认使用user.ini配置的IniRealm
* user.ini帐号admin,密码admin

##### 配置
```
project:
    dictScanner: true #是否启用字典枚举扫描
    dictScannerPackages: com.feitian
    exceptionAdvice: true #是否启用异常通知
    crossDomain: true #是否需要跨域
    responseWrap: true #是否需要自动包装response返回内容
    swagger: true #是否启用swagger
    useBaseMapper: true #是否启用baseMapper
    mapperPackages: com.feitian.sample.mapper #启用baseMapper时扫描包路径
    shiro: true #是否启用shiro

swagger:
    title: My-API
    description: 描述
    version: 1.0.0 #版本

```

##### 数据源
* 若项目中引入druid包，则会自动创建druid.DataSource,如需要覆盖参数请参照以下配置
```
druid:
  initialSize: 10 
  #最小连接池数量 
  minIdle: 10 
  #最大连接池数量 
  maxActive: 100 
  maxWait: 60000 
  #秒空闲后释放连接，以避免连接数过多 
  removeAbandonedTimeout: 180
  timeBetweenEvictionRunsMillis: 60000
  minEvictableIdleTimeMillis: 300000
  validationQuery: SELECT 1 FROM DUAL
  testWhileIdle: true
  testOnBorrow: false
  testOnReturn: false
  poolPreparedStatements: true
  maxPoolPreparedStatementPerConnectionSize: 20
  filters: stat,wall,log4j,config
  connectionProperties: config.decrypt=true;druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
```

### 多数据源配置
1. project.multipleDataSource: true
2. 创建MultipleDataSource Bean

```$xslt
@Configuration
@EnableConfigurationProperties({PrimaryProperties.class, SecondaryProperties.class, DruidProperties.class})
public class MConfig {

    @Autowired
    private PrimaryProperties primaryProperties;

    @Autowired
    private SecondaryProperties secondaryProperties;

    @Autowired
    private DruidProperties druidProperties;

    public DataSource primary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(primaryProperties).build();
    }

    public DataSource secondary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(secondaryProperties).build();
    }

    @Bean
    public MultipleDataSource getMultipleDataSource() {
        MultipleDataSource ds = new MultipleDataSource();
        ds.registor("primaryDS", primary());
        ds.registor("secondaryDS", secondary());
        ds.setDefaultKey("primaryDS");
        return ds;
    }
}
```
3. 使用

```$xslt

   //使用注解切换数据源
   @TargetDataSource("primaryDS")
    public List<Map> get1() {
        List<Map> select = mapper.select();
        return select;
    }
```


#### Utils工具(ObjectUtils)
1. 类似Groovy?.操作符，对于访问时空指针异常等其它异常，返回一个默认值
```$xslt
        final List<String> list = Arrays.asList("a", "b", "c");
        String x = ObjectUtils.safeCall(() -> list.get(5), "返回一个默认值");
```

#### 断言工具类(Assert)
* 断言并抛出指定的ErrorCode业务异常
```$xslt
Assert.isEmpty(someObject,ErrorCode)
```

#### 签名基类(Signable)
* 实现该接口，可获得签名及验签功能


#### 日期工具类(DateUtils)
* 常用日期转换方法


#### 反射工具类(ReflectionHelper)
* 提供射工具方法