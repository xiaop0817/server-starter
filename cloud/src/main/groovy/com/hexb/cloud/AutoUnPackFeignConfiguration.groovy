package com.hexb.cloud

import feign.codec.Decoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.web.HttpMessageConverters
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * @Package : com.hexb.core
 * @Author : hexb 
 * @Date : 2018-08-14 16:57
 */
@Configuration
class AutoUnPackFeignConfiguration {

    @ConditionalOnProperty(value = 'project.responseWrap', havingValue = 'true')
    @Bean
    @Autowired
    Decoder createDecoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new ResponseEntityDecoder(new ResponseFeignDecoder(messageConverters))
    }

}
