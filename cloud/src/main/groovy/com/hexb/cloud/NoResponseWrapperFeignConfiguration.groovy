package com.hexb.cloud

import feign.codec.Decoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.web.HttpMessageConverters
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder
import org.springframework.cloud.netflix.feign.support.SpringDecoder
import org.springframework.context.annotation.Bean

/**
 * @Package : com.hexb.core
 * @Author : hexb 
 * @Date : 2018-08-14 16:59
 */
class NoResponseWrapperFeignConfiguration {

    @Autowired
    private ObjectFactory<HttpMessageConverters> messageConverters;

    @Bean
    Decoder createDecoder() {
        return new ResponseEntityDecoder(new SpringDecoder(this.messageConverters))
    }

}
