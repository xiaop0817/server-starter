package com.hexb.test;

import com.hexb.core.utils.ReflectionHelper;
import com.hexb.sample.entry.Food;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @Package : com.hexb.test
 * @Author : hexb
 * @Date : 2018-08-15 10:49
 */
@RunWith(JUnit4.class)
public class ReflectionTest {

    @Test
    public void testFields() {
        List<Field> fields = ReflectionHelper.getClassFields(Food.class);
        fields.stream().forEach(System.out::println);
    }
}
