package com.hexb.test;

import com.hexb.core.model.enums.BusinessExceptionEnum;
import com.hexb.core.utils.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

/**
 * @Package : com.hexb.test
 * @Author : hexb
 * @Date : 2018-08-30 15:53
 */
@RunWith(JUnit4.class)
public class AssertTest {


    @Test
    public void testNotConstains01() {

        Assert.notMobileNoForCN("1592843x6227", MyEmuns.NO_A, "15928436227");

        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("C");
        list.add("D");

        //Assert.isContain(list, "A", MyEmuns.NO_A);
        //Assert.notContain(list, "B", MyEmuns.NOT_B);
        Assert.notContain(list, (i) -> {
            return "B".equals(i);
        }, MyEmuns.NOT_B);
    }

    enum MyEmuns implements BusinessExceptionEnum {
        NO_A("no_a", "no_A"),
        NOT_B("NOT_B", "NOT_B");

        private String message;
        private String code;
        private String key;

        MyEmuns(String message, String code) {
            this(message, code, code);
        }

        MyEmuns(String message, String code, String key) {
            this.message = message;
            this.code = code;
            this.key = key;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public String getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
