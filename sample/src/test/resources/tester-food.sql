#餐食
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food` (
  id         INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(50)  NOT NULL COMMENT '餐食名称',
  picture    VARCHAR(200) NOT NULL COMMENT '餐食图片',
  code       VARCHAR(20)  NULL COMMENT '餐食编码',
  type INT DEFAULT 0,
  grade INT DEFAULT 1,
  createTime DATETIME                          DEFAULT now() COMMENT '记录创建时间',
  updateTime TIMESTAMP COMMENT '最后一次修改时间'
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'蛋糕', 'cake.png', '001');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'冷盘', 'colddish.png', '002');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'果仁', 'nut.png', '003');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'糖果', 'sweets.png', '004');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'水果切', 'fruits.png', '005');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'广式鸳鸯奶茶', 'milkytea.png', '006');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'九彩奶茶', 'ninemilktea.png', '007');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'养生花茶', 'flowertea.png', '008');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'农夫山泉', 'water.png', '009');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'雪碧', 'sprite.png', '010');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'可口可乐', 'coke.png', '011');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'橙汁', 'juice.png', '012');
INSERT INTO food (id,name, picture, code) VALUES (replace(uuid(),'-',''),'三合一咖啡', 'coffee.png', '013')