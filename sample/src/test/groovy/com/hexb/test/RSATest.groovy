package com.hexb.test

import com.hexb.core.enums.AscColors
import com.hexb.core.utils.ObjectHelper
import com.hexb.core.utils.RSAUtils
import com.hexb.sample.entry.Food
import com.hexb.sample.entry.Person
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import static com.hexb.core.utils.RSAUtils.genKeyPair

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-17 11:35
 */
@RunWith(JUnit4.class)
class RSATest {


    @Test
    void testReflectionUtils() {
        def pair = genKeyPair()

        println "公钥：${pair.getPublicKeyString()}"
        println "私钥：${pair.getPrivateKeyString()}"

        def s = "一个中国人的skdfljdkflk困；"



        def sign = RSAUtils.sign(s, pair.getPrivateKeyString())
        println sign

        println RSAUtils.verify(s, pair.getPublicKeyString(), sign)



        Food f = new Food()
        f.setCode("k")
        f.setFoodName('萝卜')
        f.setPhoto('hhhhhhhhh')

        def properties = ObjectHelper.getSortedProperties(f)
        println properties
        println ObjectHelper.getSortedPropertiesString(f)


        println ObjectHelper.getSortedPropertiesString(new Person())


        println "${AscColors.RED} =======> ${AscColors.END} 开始加密"
        String pt = "张三李四好朋友"
        String es = RSAUtils.encryptByPublicKey(pt, pair.publicKeyString)
        println "加密后：${es}"
        println RSAUtils.decryptByPrivateKey(es, pair.privateKeyString)
    }

    @Test
    void testString() {
        String pri = 'MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANZTLeD2Uj1dkC/QWCzuPUmZ99zV7LJ8DxcRl42GtOalkO4cO4He/o8EZLRdg40Z19dCS4ZbjjsAL9SXS4maUBza59bI5eyByC2PbmOF3ddz2vlYz1CBThIMr6PK5vJzXWEOTA1CoYQ96mEgQ7FpzXFImfyuXaUY0S4w3M1m5CcxAgMBAAECgYAP/eDcekSko4iQqMELiTM1fLGL3sJshukxVXSzcKuq0gvAWIbwJ81iqndBJgEc1CLQwveTrN0t9dObzzYprEPx4EVQ+J2LgBd/SsKH+loU9ppDauYvFMjPaawMh+V9M9dDnp0FmI5SiBnZbUbhrElBil3HDX9biSpDvIxomKe1qQJBAO7FkRb7WLPdwRicrQe4D5xZJ3KsI4uNiYhhEUQOhwESctCPVX/8sOKOrH/9vSS0dsvUVGLckiU2m2Ogo0iqVhcCQQDlygwBxy54bOqeTTluOsHWgCTDZY8ENPQFpNZgoKjxRgmuDOjja5m/b09Rn/MOmmaqdWFYDBDvUkflafb3kwH3AkAArIwZTYJVl6Wo5SBT6kAZB+NlnnjHNxIRUXzT/VqCNUQftJfgMZ1qNk7kWZoEFYJUVvgaA1diRYIwyM5NMTXnAkA3zVALIn6Tegejt6cpsJUf7PCAjuojIGTd9gClaH5+UP7KyJHJoHiut24wqUISGsaxwF2KTyvRok6ZVJlH1+hTAkBbkoKCbjgUQQSOBx0QOnk/ha44o2MFkomCSibHdq4PieHtf3hyGNLUfuhWkDFa0/rqLVBEx54DdQXVUGA3bLJy' +
                'Bd1gTxJAJJfr+8KhnT9oV3L3lenkw2DH3xoW/9W5detuc3lXiSgoudk9YOIdmIEKtpKEMKPN1c4VXVwYpLq1R4AoR3tnMEvSbgl0HWj+xc0jq9gM5r5gVCQZ66S75Ll7uWLCcqR04+5GAYQZZUfn/U/PQ08wQjLXUEfwrfXXFY0='

        String pub = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWUy3g9lI9XZAv0Fgs7j1Jmffc1eyyfA8XEZeNhrTmpZDuHDuB3v6PBGS0XYONGdfXQkuGW447AC/Ul0uJmlAc2ufWyOXsgcgtj25jhd3Xc9r5WM9QgU4SDK+jyubyc11hDkwNQqGEPephIEOxac1xSJn8rl2lGNEuMNzNZuQnMQIDAQAB'

        println "${AscColors.RED} =======> ${AscColors.END} 开始加密"

        println pri

        String pt = "123456"
        String es = RSAUtils.encryptByPublicKey(pt, pub)
        println "加密后：${es}"
        println RSAUtils.decryptByPrivateKey(es, pri)


    }


    @Test
    void testString2() {
        String pri = 'MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANZTLeD2Uj1dkC/QWCzuPUmZ99zV7LJ8DxcRl42GtOalkO4cO4He/o8EZLRdg40Z19dCS4ZbjjsAL9SXS4maUBza59bI5eyByC2PbmOF3ddz2vlYz1CBThIMr6PK5vJzXWEOTA1CoYQ96mEgQ7FpzXFImfyuXaUY0S4w3M1m5CcxAgMBAAECgYAP/eDcekSko4iQqMELiTM1fLGL3sJshukxVXSzcKuq0gvAWIbwJ81iqndBJgEc1CLQwveTrN0t9dObzzYprEPx4EVQ+J2LgBd/SsKH+loU9ppDauYvFMjPaawMh+V9M9dDnp0FmI5SiBnZbUbhrElBil3HDX9biSpDvIxomKe1qQJBAO7FkRb7WLPdwRicrQe4D5xZJ3KsI4uNiYhhEUQOhwESctCPVX/8sOKOrH/9vSS0dsvUVGLckiU2m2Ogo0iqVhcCQQDlygwBxy54bOqeTTluOsHWgCTDZY8ENPQFpNZgoKjxRgmuDOjja5m/b09Rn/MOmmaqdWFYDBDvUkflafb3kwH3AkAArIwZTYJVl6Wo5SBT6kAZB+NlnnjHNxIRUXzT/VqCNUQftJfgMZ1qNk7kWZoEFYJUVvgaA1diRYIwyM5NMTXnAkA3zVALIn6Tegejt6cpsJUf7PCAjuojIGTd9gClaH5+UP7KyJHJoHiut24wqUISGsaxwF2KTyvRok6ZVJlH1+hTAkBbkoKCbjgUQQSOBx0QOnk/ha44o2MFkomCSibHdq4PieHtf3hyGNLUfuhWkDFa0/rqLVBEx54DdQXVUGA3bLJy' +
                'Bd1gTxJAJJfr+8KhnT9oV3L3lenkw2DH3xoW/9W5detuc3lXiSgoudk9YOIdmIEKtpKEMKPN1c4VXVwYpLq1R4AoR3tnMEvSbgl0HWj+xc0jq9gM5r5gVCQZ66S75Ll7uWLCcqR04+5GAYQZZUfn/U/PQ08wQjLXUEfwrfXXFY0='

        String pub = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWUy3g9lI9XZAv0Fgs7j1Jmffc1eyyfA8XEZeNhrTmpZDuHDuB3v6PBGS0XYONGdfXQkuGW447AC/Ul0uJmlAc2ufWyOXsgcgtj25jhd3Xc9r5WM9QgU4SDK+jyubyc11hDkwNQqGEPephIEOxac1xSJn8rl2lGNEuMNzNZuQnMQIDAQAB'

        String e = 'yiysQQsFwR8+jBmTsuw+6+QsYxyLlqT41PSJnqTxthMdjERBo6bmjS5AU93jxxcF8+sU4zPbzZNKvCY+6F3t5zHuJbi/S3dxdeWNtPFtk9DTM0dVdsOv95uuoMwQZiobPl+rf8AxGN1HNEUHW8pSCU1BLKJ/AO/tFdohQXZDSlE='
        println "0 解密后：======>>"
        println RSAUtils.decryptByPrivateKey(e, pri)


        String e1 = 'N6+eQSJLvpbzb4XeMYo+X2gk2v1vGzKQNpiZJFlbFf9ncfYpJwllS2iahsmMxMF6cItivjKftzP8DyCh97GaOi6hDcG9IZCMt72/cnv7rj0IlPd5h90MiztVY6YrxgvyvAzQRYCR/nmNqCJBTN1G4byrJb/hBQCaJzqiqENam1I='
        println "1 解密后：======>>"
        println RSAUtils.decryptByPrivateKey(e1, pri)


        String e2 = 'gRQ+tRTpsoEEpODJCxhw2VrWX1WYDfREh4wWO/rW8k2CWRJafeiisVQWS/SXSQ7tLSUL0hN2GHGLuU2VpcSpS0shDeI0v1z2LPAiIgL1rAI++wn2HKN6a01D3v13IEVubRr5r0R8jwdBz4JP1M4MIRkRHa6qifSiF6peHd9BDvM='
        println "2 解密后：======>>"
        println RSAUtils.decryptByPrivateKey(e2, pri)


        String e3 = 'RCspmtjNHfgmiuGSpNRm8xK14yVTBF2qbAu2VO9dP1TjBzb0nA8NCeaMEERZcSZHtbZDVR/KJuyIAwFz93CgMmqdOhPVMzX+ATqMDvIZfwntGcxmOpIOQtxX5F+EPzDjeGsRZQDG0AGWtWAuYLg7gIqkYLwCkCb9EZmIOTGaTR8='
        println "3 解密后：======>>"
        println RSAUtils.decryptByPrivateKey(e3, pri)

        println "公鈅：======>>"
        println pub


        println "私鈅：======>>"
        println pri
    }

}
