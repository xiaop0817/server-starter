package com.hexb.test

import com.hexb.core.utils.RegexUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.Assert

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-17 11:35
 */
@RunWith(JUnit4.class)
class RegexTest {


    @Test
    void testRegex() {
        def ips = ['192.168.1.100', '192.1368aa', '192.168.257.1', '192.168.255.0']
        Assert.assertTrue(ips[0].matches(RegexUtils.IP4))
        Assert.assertFalse(ips[1].matches(RegexUtils.IP4))
        Assert.assertFalse(ips[2].matches(RegexUtils.IP4))
        Assert.assertTrue(ips[3].matches(RegexUtils.IP4))

        def macs = ['14:25:11:AF:00:08', '14-25-11-AF-00-08','14:25:11:AF:00:08x']
        Assert.assertTrue(macs[0].matches(RegexUtils.MAC))
        Assert.assertTrue(macs[1].matches(RegexUtils.MAC))
        Assert.assertFalse(macs[2].matches(RegexUtils.MAC))
        println macs[2].matches(RegexUtils.MAC)

        println '13910733521'.matches(RegexUtils.MOBILE_CN)


        def ipv6s = ['2001:0DB8:0000:0000:0000:0000:1428:57ab','2001:0DB8:0000:0000:0000::1428:57ab']

        println "------------"

        println ips[0].matches(RegexUtils.IP)

        println ipv6s[0].matches(RegexUtils.IP)
        println ipv6s[1].matches(RegexUtils.IPv6)
        def ss = UUID.randomUUID().toString().replaceAll("-","");
        println ss
        println ss.length()
    }


}
