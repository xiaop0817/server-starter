package com.hexb.test

import com.hexb.core.mybatis.entry.BatchUpdateParam
import com.hexb.sample.SampleApplication
import com.hexb.sample.entry.Food
import com.hexb.sample.enums.FoodGrade
import com.hexb.sample.enums.FoodType
import com.hexb.sample.mapper.FoodMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-16 09:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = [SampleApplication.class])
class MapperTest {

    @Autowired
    private FoodMapper foodMapper


    @Test
    void baseMapper_select_Test() {
        String id = 'c44c9e20916911e88aeecfba37e3cc54'
        def list = foodMapper.select(new Food(id: id))
        println('========================== select =====================')
        list.each {
            println(it)
        }
    }

    @Test
    void baseMapper_selectOne_Test() {
        String id = 'c44c9e20916911e88aeecfba37e3cc54'
        def one = foodMapper.selectOne(id)
        println('========================== selectOne =====================')
        println(one)
        Assert.assertEquals(one.id, id)
    }


    @Test
    void baseMapper_count_Test() {
        def list = foodMapper.select([:])
        def count = foodMapper.count([:])
        println('========================== count all=====================')
        println(count)
        Assert.assertEquals(count, list.size())

        println('========================== count [id:c44c9e20916911e88aeecfba37e3cc54] =====================')
        String id = 'c44c9e20916911e88aeecfba37e3cc54'
        count = foodMapper.count([id: id])
        println(count)
        Assert.assertEquals(count, 1)
    }

    @Test
    void baseMapper_insert_Test() {
        def before = foodMapper.count([:])
        println('========================== insert =====================')
        println("before insert : ${before}")
        def s = new Date().time
        def food = new Food(foodName: "好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.VERY_BAD, type: FoodType.MEAT)
        foodMapper.insert(food)
        def after = foodMapper.count([:])
        println("after insert : ${after}")

        println("food id ===> ${food.id}")
        Assert.assertNotNull(food)
        Assert.assertEquals(before + 1, after)
        foodMapper.delete(food.id)
    }


    @Test
    void baseMapper_insertBatch_Test() {
        def before = foodMapper.count([:])
        println('========================== insert =====================')
        println("before insert : ${before}")
        def s = new Date().time
        def food = new Food(foodName: "1:好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.VERY_BAD, type: FoodType.VEGETABLE)
        s += 100
        def food2 = new Food(foodName: "2:好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.GOOD, type: FoodType.MEAT)

        foodMapper.insertBatch([food, food2])
        def after = foodMapper.count([:])
        println("after insert : ${after}")

        println("food id ===> ${food.id}")
        println("food2 id ===> ${food2.id}")

        Assert.assertNotNull(food)
        Assert.assertEquals(before + 2, after)
        foodMapper.deleteBatch([food.id, food2.id].toArray())
    }

    @Test
    void baseMapper_update_Test() {
        String id = 'c4507b6c916911e88aeecfba37e3cc54'
        String newName = '被更新过的名称'
        def food = foodMapper.selectOne(id)
        def oldName = food.foodName
        food.type = FoodType.VEGETABLE
        food.grade = FoodGrade.VERY_BAD
        food.foodName = newName
        foodMapper.update(food)
        def food2 = foodMapper.selectOne(id)
        Assert.assertEquals(food2.foodName, newName)

        food2.foodName = oldName
        foodMapper.update(food2)
        def food3 = foodMapper.selectOne(id)
        Assert.assertEquals(food2.foodName, oldName)
    }


    @Test
    void baseMapper_updateBatch_Test() {
        def food = new Food()
        food.type = FoodType.VEGETABLE
        food.grade = FoodGrade.VERY_BAD
        food.foodName = '美味风蛇'
        def param = new BatchUpdateParam<String, Food>()
        param.ids = ['1', '2', '3']
        param.entry = food
        foodMapper.updateBatch(param)
        def foods = foodMapper.selectByMap([foodName: '美味风蛇'])
        Assert.assertEquals(foods.size(), 3)
        Assert.assertArrayEquals(foods.collect { it.id }.toArray(), ['1', '2', '3'].toArray())
    }


    @Test
    void baseMapper_deleteBatch_Test() {

        def s = new Date().time
        def food = new Food(foodName: "批量删除的菜1", photo: "${s}.png", code: s, grade: FoodGrade.VERY_BAD, type: FoodType.VEGETABLE)
        s += 100
        def food2 = new Food(foodName: "批量删除的菜2", photo: "${s}.png", code: s, grade: FoodGrade.GOOD, type: FoodType.MEAT)

        foodMapper.insertBatch([food, food2])

        def list = foodMapper.select([id: food.id])
        Assert.assertEquals(list.size(), 1)

        def list2 = foodMapper.select([id: food2.id])
        Assert.assertEquals(list2.size(), 1)

        foodMapper.deleteBatch([food.id, food2.id].toArray())

        list = foodMapper.select([id: food.id])
        Assert.assertEquals(list.size(), 0)

        list2 = foodMapper.select([id: food2.id])
        Assert.assertEquals(list2.size(), 0)
    }


    @Test
    void baseMapper_delete_Test() {
        def food = new Food(foodName: "看不再的菜", photo: "hidden.png", code: '88888', grade: FoodGrade.VERY_BAD, type: FoodType.MEAT)
        foodMapper.insert(food)
        foodMapper.delete(food.id)
        def list = foodMapper.select([id: food.id])
        Assert.assertEquals(list.size(), 0)
    }


    @Test
    void baseMapperTest() {
        def list = foodMapper.select([name: '三合一咖啡'])
        println('========================== select =====================')
        list.each {
            println("$it ==> ${it.createTime}")
        }
        Assert.assertNotNull(list)
    }
}
