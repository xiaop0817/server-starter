package com.hexb.test


import com.hexb.core.scanner.DictionaryScanner
import com.hexb.core.utils.ObjectHelper
import com.hexb.core.utils.ReflectionHelper
import com.hexb.sample.entry.Food
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import java.text.MessageFormat

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-17 11:35
 */
@RunWith(JUnit4.class)
class UtilsTest {


    void testReflectionUtils() {
        ReflectionHelper.getClassFields(Food.class).each {
            println it.name
        }

        String s = 'A String'
        println("$s")
    }


//    @Test
    void testDict() {
        def dic = new DictionaryScanner("com.hexb.sample.enums")
        dic.scanner()
        println(dic.list)
    }

    @Test
    void testObjectHelper(){
        def f = new Food(foodName:'哈哈')
        def s = ObjectHelper.getProperty(f,'foodName2')
        println s


        println MessageFormat.format('乘客身份服务配置缺失({0})','A','B','C','D','E')
    }
}
