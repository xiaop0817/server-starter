package com.hexb.test

import com.hexb.sample.SampleApplication
import com.hexb.sample.entry.Food
import com.hexb.sample.enums.FoodGrade
import com.hexb.sample.enums.FoodType
import com.hexb.sample.mapper.FoodMapper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-16 09:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = [SampleApplication.class])
class MapperInsertTest {

    @Autowired
    private FoodMapper foodMapper


    @Test
    void testTypeHandler() {
        def food = new Food(foodName: "TypeHandler", foodId: '1231231313',
                photo: "TypeHandler.png",
                code: "258",
                type: FoodType.MEAT)
        foodMapper.insert(food)
        println "------------------------------------------"
    }

    //@Test
    void baseMapper_insert_Test() {
        def before = foodMapper.count([:])
        println('========================== insert =====================')
        println("before insert : ${before}")
        def s = new Date().time
        def food = new Food(foodName: "好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.VERY_BAD, type: FoodType.MEAT)
        foodMapper.insert(food)
        def after = foodMapper.count([:])
        println("after insert : ${after}")

        println("food id ===> ${food.id}")
        Assert.assertNotNull(food)
        Assert.assertEquals(before + 1, after)
        foodMapper.delete(food.id)
    }

    //@Test
    void baseMapper_insertBatch_Test() {
        def before = foodMapper.count([:])
        println('========================== insert =====================')
        println("before insert : ${before}")
        def s = new Date().time
        def food = new Food(foodName: "1:好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.VERY_BAD, type: FoodType.VEGETABLE)
        s += 100
        def food2 = new Food(foodName: "2:好吃的${s}", photo: "${s}.png", code: s, grade: FoodGrade.GOOD, type: FoodType.MEAT)

        foodMapper.insertBatch([food, food2])
        def after = foodMapper.count([:])
        println("after insert : ${after}")

        println("food id ===> ${food.id}")
        println("food2 id ===> ${food2.id}")

        Assert.assertNotNull(food)
        Assert.assertEquals(before + 2, after)
    }

}
