package com.hexb.test

import com.hexb.core.security.Signable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import static com.hexb.core.utils.RSAUtils.genKeyPair

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-9 15:06
 */
@RunWith(JUnit4.class)
class KeyPairGen {

    @Test
    void testReflectionUtils() {
        def pair = genKeyPair()

        println "公钥：${pair.getPublicKeyString()}"
        println "私钥：${pair.getPrivateKeyString()}"
    }

    //@Test
    void testObjectHelper(){
        def pair = genKeyPair()
        User u = new User()
        println u.original()
        //println u.sign(pair.getPrivateKeyString())
    }

    class User implements Signable{
        String name = 'Name'
        String a = 'A'
        String b = 'B'
        String d = 'C'
        String signStr = 'signStr'

        @Override
        String[] excludeFields() {
            return ['signStr']
        }
    }
}
