package com.hexb.test


import com.hexb.core.scanner.DictionaryScanner
import com.hexb.core.utils.ReflectionHelper
import com.hexb.sample.entry.Food
import com.hexb.sample.enums.TestEnum
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-17 11:35
 */
@RunWith(JUnit4.class)
class EnumTest {


    @Test
    void testReflectionUtils() {
        TestEnum value = TestEnum.byValue(TestEnum.class, 2)
        println value.name()
    }


}
