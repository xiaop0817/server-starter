package com.hexb.test

import com.hexb.sample.SampleApplication
import com.hexb.sample.PassengerService
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * @Package : com.hexb.test
 * @Author : hexb 
 * @Date : 2018-08-7 16:46
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = [SampleApplication.class])
class DSTest {


    @Autowired
    private PassengerService service;

    @Test
    void test() {
        def get1 = service.get1()
        println get1

        println '--------------------'

        def get2 = service.get2()
        println get2


    }
}
