package com.hexb.sample.mapper

import com.hexb.core.mapper.BaseMapper
import com.hexb.sample.entry.Food
import org.apache.ibatis.annotations.Mapper

/**
 * @Package : com.hexb.sample.mapper
 * @Author : hexb 
 * @Date : 2018-08-13 14:01
 */
@Mapper
interface FoodMapper extends BaseMapper<String, Food> {

}
