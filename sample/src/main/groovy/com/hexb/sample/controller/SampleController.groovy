package com.hexb.sample.controller


import com.hexb.core.exception.BusinessException
import com.hexb.core.utils.Assert
import com.hexb.sample.entry.Food
import com.hexb.sample.enums.FoodErrorCode
import com.hexb.sample.enums.TestEnum
import com.hexb.sample.mapper.FoodMapper
import com.hexb.sample.FoodService
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

/**
 * @Package : com.hexb.sample.controller
 * @Author : hexb 
 * @Date : 2018-08-12 14:05
 */
@RestController
@RequestMapping('/rest/sample')
class SampleController {


    @Autowired
    private FoodMapper foodMapper

    @Autowired
    private FoodService foodService

    @GetMapping('/test')
    String test() {
        "test ${new Date().time}"
    }

    @GetMapping('/error')
    String error() {
        throw new BusinessException('一个错误', '900')
    }


    @GetMapping('/assert')
    String testAssert() {
        Assert.notNull(null, "123|错误的参数")
    }


    @GetMapping('/foods')
    List<Food> foods() {
        foodMapper.select([_name: '果'])
    }

    @PostMapping("/post/test")
    Food postTest() {
        new Food(foodName: '萝卜', photo: 'http://123.png')
    }


    @GetMapping('/list')
    @RequiresPermissions('admin:list')
    List<Food> list() {
        foodMapper.select([:])
    }

    @PostMapping('/append')
    @RequiresPermissions('admin:create')
    Food create(@Valid @RequestBody Food food) {
        println food
        //foodMapper.insert(food)
    }

    @GetMapping("/service")
    void testService() {
        foodService.select(new HashMap())
    }


    @GetMapping("/i18n")
    void testI18n() {
        throw new BusinessException("哈哈这个不会被国际化", "text")
    }

    @GetMapping("/enums")
    void testEnums() {
        throw new BusinessException(FoodErrorCode.X)
    }


    @GetMapping("/enumsValue")
    void tesEnumsValue() {
        TestEnum.byValue(TestEnum.class, 2)
    }


}
