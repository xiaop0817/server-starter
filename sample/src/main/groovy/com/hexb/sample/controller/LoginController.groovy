package com.hexb.sample.controller

import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.authz.annotation.RequiresAuthentication
import org.apache.shiro.subject.Subject
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * @Package : com.hexb.sample.controller
 * @Author : hexb 
 * @Date : 2018-08-23 14:40
 */
@RestController
@RequestMapping('/auth')
class LoginController {

    @PostMapping("/login")
    Object login(@RequestParam String name, @RequestParam String password) {
        Subject subject = SecurityUtils.getSubject()
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(name, password)
        subject.login(usernamePasswordToken)
        subject.getPrincipal()
    }

    @PostMapping("/logout")
    @RequiresAuthentication
    String logout() {
        Subject subject = SecurityUtils.getSubject()
        subject.logout()
        "logout"
    }


    @GetMapping("/testNull")
    String testNull(){
        return null
    }
}
