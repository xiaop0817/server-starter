package com.hexb.sample

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.EnableAspectJAutoProxy

/**
 * @Package : com.hexb.sample
 * @Author : hexb 
 * @Date : 2018-08-12 14:04
 */
@SpringBootApplication(scanBasePackages = ['com.hexb.core','com.hexb.sample'])
//@MapperScan(['com.hexb.sample.mapper','com.hexb.sample'])
class SampleApplication {

    static void main(String[] args) {
        SpringApplication.run(SampleApplication.class, args)
    }
}
