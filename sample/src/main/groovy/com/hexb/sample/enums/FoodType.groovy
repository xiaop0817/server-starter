package com.hexb.sample.enums

import com.hexb.core.annotation.Dict

/**
 * @Package : com.hexb.sample.enums
 * @Author : hexb 
 * @Date : 2018-08-18 16:22
 */
@Dict
enum FoodType {

    VEGETABLE, MEAT



}