package com.hexb.sample.enums

import com.hexb.core.annotation.Dict
import com.hexb.core.model.ValueEnum

/**
 * @Package : com.hexb.sample.enums
 * @Author : hexb 
 * @Date : 2018-08-18 17:02
 */
@Dict
enum FoodGrade implements ValueEnum<FoodGrade> {

    GOOD(1, '好吃'),
    BAD(2, '不球好吃'),
    VERY_BAD(3, '好球难吃啊')

    private int value
    private String description

    FoodGrade(int value, String description) {
        this.value = value
        this.description = description
    }

    @Override
    int getValue() {
        return value
    }

    @Override
    String getDescription() {
        return description
    }
}
