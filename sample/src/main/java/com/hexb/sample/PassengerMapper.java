package com.hexb.sample;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Package : com.hexb.sample.mapper
 * @Author : hexb
 * @Date : 2018-08-7 16:43
 */
@Mapper
@Repository
public interface PassengerMapper {

    @Select("select * from passenger")
    List<Map> select();
}
