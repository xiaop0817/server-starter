package com.hexb.sample.enums;

import com.hexb.core.annotation.ErrorCode;
import com.hexb.core.model.enums.BusinessExceptionEnum;

/**
 * @Package : com.hexb.sample.enums
 * @Author : hexb
 * @Date : 2018-08-6 15:12
 */
@ErrorCode(module = "测试", name = "XYZ", description = "一个错误的测试")
public enum FoodErrorCode implements BusinessExceptionEnum {
    X("X", "一个X错误"),
    Y("Y", "一个Y错误"),
    Z("Z", "一个Z错误"),
    ;

    private String code;
    private String message;

    FoodErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return "test." + this.code.toLowerCase();
    }


}
