package com.hexb.sample.enums;

import com.hexb.core.model.ValueEnum;

/**
 * @Package : com.hexb.sample.enums
 * @Author : hexb
 * @Date : 2018-08-10 17:26
 */
public enum TestEnum implements ValueEnum<TestEnum> {

    RED(1, "#ff0000");

    TestEnum(int value, String description) {
         this.value = value;
         this.description = description;
     }

     private int value;
     private String description;

     @Override
     public int getValue() {
         return value;
     }

     @Override
     public String getDescription() {
         return description;
     }
}
