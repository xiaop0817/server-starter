package com.hexb.sample;

import com.hexb.core.mapper.BaseMapper;
import com.hexb.core.model.QueryParameter;
import com.hexb.core.service.AbstractBaseService;
import com.hexb.sample.entry.Food;
import org.springframework.stereotype.Service;

/**
 * @Package : com.hexb.sample.service
 * @Author : hexb
 * @Date : 2018-08-30 17:09
 */
@Service
public class FoodService extends AbstractBaseService<String, Food, QueryParameter> {

    @Override
    protected BaseMapper<String, Food> getDefaultMapper() {
        return null;
    }

}
