package com.hexb.sample;

import com.hexb.core.jdbc.TargetDataSource;
import com.hexb.sample.PassengerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Package : com.hexb.sample.service
 * @Author : hexb
 * @Date : 2018-08-7 16:44
 */
@Service
public class PassengerService {

    @Autowired
    private PassengerMapper mapper;

    @TargetDataSource("primaryDS")
    public List<Map> get1() {
        List<Map> select = mapper.select();
        return select;
    }


    @TargetDataSource("primarySE")
    public List<Map> get2() {
        List<Map> select = mapper.select();
        return select;
    }
}
