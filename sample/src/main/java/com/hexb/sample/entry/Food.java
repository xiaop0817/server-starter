package com.hexb.sample.entry;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hexb.core.annotation.Column;
import com.hexb.core.annotation.Id;
import com.hexb.core.annotation.Table;
import com.hexb.core.model.enums.IDType;
import com.hexb.sample.enums.FoodType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Package : com.hexb.sample.entry
 * @Author : hexb
 * @Date : 2018-08-15 10:41
 */
@Table(idType = IDType.NO_KEY)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Food {

    @Id("id")
    private String foodId ;

    @Column(value = "name")
    @NotNull(message = "名称不能为空")
    @ApiModelProperty(example = "萝卜")
    private String foodName;

    @Column("picture")
    private String photo;

    @NotNull(message = "编码不能为空")
    private String code;

    private FoodType type;

}
