package com.hexb.sample.entry;

import lombok.Data;

/**
 * @Package : com.hexb.sample.entry
 * @Author : hexb
 * @Date : 2018-08-17 18:17
 */
@Data
public class Person {

    private String aA = "不知道"  ;
    private String c  = "不知道"  ;
    private String d  = "不知道"  ;
    private String e  = "不知道"   ;
    private String hx = "不知道"  ;
    private String H  = "不知道"  ;
    private String X  = "不知道"  ;
    private String Aa = "不知道"   ;
    private String C  = "不知道"  ;

}
