package com.hexb.sample.config;

import com.hexb.core.common.IDataSourceProperties;
import com.hexb.core.properties.DataSourceProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Package : com.hexb.sample.config
 * @Author : hexb
 * @Date : 2018-08-7 16:34
 */
@ConfigurationProperties("multipleDataSource.primary")
@Data
public class PrimaryProperties implements IDataSourceProperties {
    String url;

    String username;

    String password;

    String driverClassName;
}
