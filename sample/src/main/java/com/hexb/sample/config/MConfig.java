package com.hexb.sample.config;

import com.hexb.core.jdbc.MultipleDataSource;
import com.hexb.core.properties.DruidProperties;
import com.hexb.core.utils.DruidDataSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @Package : com.hexb.sample.config
 * @Author : hexb
 * @Date : 2018-08-7 16:32
 */
@Configuration
@EnableConfigurationProperties({PrimaryProperties.class, SecondaryProperties.class, DruidProperties.class})
public class MConfig {

    @Autowired
    private PrimaryProperties primaryProperties;

    @Autowired
    private SecondaryProperties secondaryProperties;

    @Autowired
    private DruidProperties druidProperties;

    public DataSource primary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(primaryProperties).build();
    }

    public DataSource secondary() {
        return new DruidDataSourceBuilder().druid(druidProperties).jdbc(secondaryProperties).build();
    }

    @Bean
    public MultipleDataSource getMultipleDataSource() {
        MultipleDataSource ds = new MultipleDataSource();
        ds.register("primaryDS", primary());
        ds.register("primarySE", secondary());
        ds.setDefaultKey("primaryDS");
        return ds;
    }

}
